FROM alpine:3.4
RUN apk add --no-cache openjdk8
VOLUME /tmp
EXPOSE 8080
ADD /target/reseller-portal-0.0.1-SNAPSHOT.jar reseller-portal-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","reseller-portal-0.0.1-SNAPSHOT.jar"]