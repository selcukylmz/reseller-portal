package com.donetr.rp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;

import com.donetr.rp.domain.Hotel;
import com.donetr.rp.domain.Region;
import com.donetr.rp.service.HotelRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase
public class HotelRepositoryTest{
	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private HotelRepository hotelRepository;

	@Test
	public void whenFindByName_thenReturnHotel() {
		//given
		Region region = new Region("Europe");
		
		Hotel hotel = new Hotel("'Test1_?!'",region);
		
		entityManager.persist(hotel);
		entityManager.flush();
		
		//when
		Hotel found = hotelRepository.findByName(hotel.getName());
		
		//then
		assertThat(found.getName())
		.isEqualTo(hotel.getName());
	}
	
	
}