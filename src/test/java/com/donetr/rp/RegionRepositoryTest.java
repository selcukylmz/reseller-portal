package com.donetr.rp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import com.donetr.rp.domain.Region;
import com.donetr.rp.service.RegionRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase
public class RegionRepositoryTest {
	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private RegionRepository regionRepository;

	@Test
	public void whenFindAll() {

		// given
		Region firstRegion = new Region();
		firstRegion.setName("Africa");
		entityManager.persist(firstRegion);
		entityManager.flush();

		Region secondRegion = new Region();
		secondRegion.setName("Asia");
		entityManager.persist(secondRegion);
		entityManager.flush();

		// when
		List<Region> regions = regionRepository.findAll();

		// then
		assertThat(regions.size()).isEqualTo(2);
		assertThat(regions.get(0)).isEqualTo(firstRegion);
		assertThat(regions.get(1)).isEqualTo(secondRegion);

	}

	@Test
	public void whenFindById() {

		// given
		Region region = new Region();
		region.setName("Africa");
		entityManager.persist(region);
		entityManager.flush();

		// when
		Region testRegion = regionRepository.findById(region.getId());
		// then
		assertThat(testRegion.getName()).isEqualTo(region.getName());

	}

	@Test
	public void whenFindByName_thenReturnRegion() {
		// given
		Region region = new Region("Europe");
		entityManager.persist(region);
		entityManager.flush();

		// when
		Region found = regionRepository.findByName(region.getName());

		// then
		assertThat(found.getName()).isEqualTo(region.getName());
	}

}