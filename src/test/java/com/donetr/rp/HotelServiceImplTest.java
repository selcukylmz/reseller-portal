package com.donetr.rp;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.donetr.rp.controller.HotelController;
import com.donetr.rp.domain.Hotel;
import com.donetr.rp.domain.Region;
import com.donetr.rp.service.HotelRepository;
import com.donetr.rp.service.HotelService;
import com.donetr.rp.service.HotelServiceImpl;
import com.donetr.rp.service.RegionRepository;

@RunWith(SpringRunner.class)

public class HotelServiceImplTest {
	@TestConfiguration
	static class HotelServiceImplTestContextConfiguration {

		@Bean
		public HotelService hotelService() {
			return new HotelServiceImpl();
		}
	}
	
	@Autowired
	private HotelService hotelService;
	
	@MockBean
	private HotelRepository hotelRepository;
	
	@MockBean
	private RegionRepository regionRepository;
	
	@Before
	public void setUp() {
	    Hotel hotel = new Hotel("Abc", new Region("Europe"));
	 
	    Mockito.when(hotelRepository.findByName(hotel.getName()))
	      .thenReturn(hotel);
	}
	
	@Test
	public void testFindByName() {
	    String name = "Abc";
	    Hotel found = hotelService.getHotelByName(name);
	  
	     assertThat(found.getName())
	      .isEqualTo(name);
	 }
	
}
