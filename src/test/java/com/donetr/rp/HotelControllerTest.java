package com.donetr.rp;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Matchers.any;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import com.donetr.rp.controller.HotelController;
import com.donetr.rp.domain.Hotel;
import com.donetr.rp.domain.Region;
import com.donetr.rp.service.HotelRepository;
import com.donetr.rp.service.HotelService;
import com.donetr.rp.service.RegionRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

import static java.util.Collections.singletonList;

@RunWith(SpringRunner.class)
@WebMvcTest(HotelController.class)
// @AutoConfigureMockMvc
public class HotelControllerTest {

	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	private MockMvc mvc;

	@MockBean
	private HotelService hotelService;

	private final String URL = "/api/";

	@Test
	public void testGetAllRegions() throws Exception {

		Region region = new Region();
		region.setName("Asia");
		region.setId(3L);

		Region region2 = new Region();
		region2.setName("Europe");
		region2.setId(4L);

		List<Region> allRegions = Arrays.asList(region, region2);

		given(hotelService.getAllRegions()).willReturn(allRegions);

		mvc.perform(get("/api/regions").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(2))).andExpect(jsonPath("$[0].name", is(region.getName())));
	}

	@Test
	public void testCreateRegion() throws Exception {
		Region mockRegion = new Region();
		mockRegion.setName("Middle East");

		when(hotelService.createRegion(any(Region.class))).thenReturn(mockRegion);

		// execute
		mvc.perform(post(URL + "regions").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(mockRegion))).andDo(print())
				.andExpect(status().isCreated())
				.andReturn();
		
		// verify that service method was called once
		verify(hotelService).createRegion(any(Region.class));

	}

	@Test
	public void testDeleteRegion() throws Exception {
		Region region = new Region("Region Test");
		region.setId(1L);
		when(hotelService.getRegionById((any(Long.class)))).thenReturn(region);

		// execute
		mvc.perform(MockMvcRequestBuilders.delete(URL + "regions/{id}", 1L))
			    .andExpect(status().isGone())
				.andReturn();

		// verify
		// int status = result.getResponse().getStatus();
		// assertEquals("Incorrect Response Status", HttpStatus.GONE.value(), status);

		// verify that service method was called once
		verify(hotelService).deleteRegion(any(Long.class));

	}

	@Test
	public void testUpdateRegion() throws Exception {
		Region region = new Region("New Region");
		region.setId(1L);

		when(hotelService.getRegionById((any(Long.class)))).thenReturn(region);

		// execute
		mvc.perform(MockMvcRequestBuilders.put(URL + "regions/{id}", 1L)
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(region)))
				.andReturn();

		// verify
		// int status = result.getResponse().getStatus();
		// assertEquals("Incorrect Response Status", HttpStatus.OK.value(), status);

		// verify that service method was called once
		verify(hotelService).updateRegion(any(Region.class));

	}

	@Test
	public void testCreateHotel() throws Exception {
		Hotel mockHotel = new Hotel();
		mockHotel.setName("La Verda");
		Region region = new Region("Europe");
		region.setId(5L);
		mockHotel.setRegion(region);
		
		when(hotelService.createHotel(any(Hotel.class))).thenReturn(mockHotel);

		// execute
		mvc.perform(post(URL+ "hotels").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(mockHotel)))
				.andDo(print())
				.andExpect(status().isCreated());
		
		
		// verify that service method was called once
		verify(hotelService).createHotel(any(Hotel.class));
	}
	

	@Test
	public void testDeleteHotel() throws Exception {
		Region region = new Region("Region Test");
		region.setId(1L);
		
		Hotel hotel = new Hotel("H x", region);
		hotel.setId(1L);
		
		when(hotelService.getHotelById((any(Long.class)))).thenReturn(hotel);

		// execute
		mvc.perform(MockMvcRequestBuilders.delete(URL + "hotels/{id}", 1L))
			    .andExpect(status().isGone())
				.andReturn();

		// verify
		// int status = result.getResponse().getStatus();
		// assertEquals("Incorrect Response Status", HttpStatus.GONE.value(), status);

		// verify that service method was called once
		verify(hotelService).deleteHotel(any(Long.class));

	}

	@Test
	public void testUpdateHotel() throws Exception {
		Region region = new Region("Region");
		region.setId(1L);

		Hotel hotel = new Hotel("New hotel name", region);
		hotel.setId(1L);
	
		when(hotelService.getHotelById((any(Long.class)))).thenReturn(hotel);

		// execute
		mvc.perform(MockMvcRequestBuilders.put(URL + "hotels/{id}", 1L)
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(hotel)))
				.andReturn();

		// verify
		// int status = result.getResponse().getStatus();
		// assertEquals("Incorrect Response Status", HttpStatus.OK.value(), status);

		// verify that service method was called once
		verify(hotelService).updateHotel(any(Hotel.class));

	}
	

}
