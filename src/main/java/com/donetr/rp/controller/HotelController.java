package com.donetr.rp.controller;

import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.bind.annotation.RequestBody;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;

import com.donetr.rp.domain.Hotel;
import com.donetr.rp.domain.Region;
import com.donetr.rp.service.HotelRepository;
import com.donetr.rp.service.HotelService;
import com.donetr.rp.service.RegionRepository;
import com.donetr.rp.util.CustomErrorType;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@EnableWebMvc
@RestController
@RequestMapping("/api")
public class HotelController {

	public static final Logger log = LoggerFactory.getLogger(HotelController.class);

	@Autowired
	HotelService hotelService;

	@Autowired
	ObjectMapper objectMapper;

	// Get All Hotels
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping("/hotels")
	public ResponseEntity<List<Hotel>> getAllHotels() {
		log.info("Getting all hotels");
		List<Hotel> hotels = hotelService.getAllHotels();
		if (hotels.isEmpty()) {
			log.error("Hotel list is empty!");
			return new ResponseEntity(new CustomErrorType("Hotel list is empty!"), HttpStatus.NOT_FOUND);
		}
		return ResponseEntity.ok().body(hotels);
	}

	// Get a single Hotel
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping("/hotels/{id}")
	public ResponseEntity<Hotel> getHotelById(@PathVariable(value = "id") Long id) {
		log.info("Fetching hotel with id {}", id);
		Hotel hotel = hotelService.getHotelById(id);
		if (hotel == null) {
			log.error("Hotel with id {} not found", id);
			return new ResponseEntity(new CustomErrorType("Hotel with id" + id + " not found."), HttpStatus.NOT_FOUND);
		}
		return ResponseEntity.ok().body(hotel);
	}

	// Get hotels in a Region
	@SuppressWarnings("unchecked")
	@GetMapping("/hotels_in_region/{id}")
	public ResponseEntity<List<Hotel>> displayHotels(Model model, @PathVariable(value = "id") Long id) {

		log.info("Getting all hotels in the region");
		List<Hotel> hotels = hotelService.getHotelsByRegion(hotelService.getRegionById(id));
		if (hotels == null) {
			log.error("There is no hotel in this region!");
			return new ResponseEntity(new CustomErrorType("There is no hotel in the region with id {}" + id),
					HttpStatus.NOT_FOUND);
		}
		return ResponseEntity.ok().body(hotels);
	}

	// Create a new Hotel
	@SuppressWarnings("unchecked")
	@PostMapping("/hotels")
	public ResponseEntity<?> addHotel(@Valid @RequestBody Hotel hotel) throws Exception {

		log.info("Creating Hotel : {}", hotel);
		String name = hotel.getName();
		if (hotelService.isHotelExist(hotel)) {
			log.error("Unable to create. Hotel with name {} already exists.", name);
			return new ResponseEntity(
					new CustomErrorType("Unable to create. Hotel with name " + name + " already exists."),
					HttpStatus.CONFLICT);
		}

		// System.out.println("Hotel JSON: "+ objectMapper.writeValueAsString(hotel));
		Hotel result = hotelService.createHotel(hotel);

		if (result == null) {
			log.error("Hotel creation failed!");
			return ResponseEntity.noContent().build();
		} else
			log.info("Created hotel with name {}", name);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(result.getId())
				.toUri();

		return ResponseEntity.created(location).build();

	}

	// Update a Hotel
	@SuppressWarnings("unchecked")
	@PutMapping("/hotels/{id}")
	public ResponseEntity<?> updateHotel(@PathVariable(value = "id") Long id, @Valid @RequestBody Hotel hotelDetails) {
		Hotel hotel = hotelService.getHotelById(id);
		if (hotel == null) {
			log.error("Unable to update. Hotel with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Unable to update. Hotel with id " + id + " not found."),
					HttpStatus.NOT_FOUND);
		}
		hotel.setName(hotelDetails.getName());
		hotel.setRegion(hotelDetails.getRegion());

		Hotel updatedHotel = hotelService.updateHotel(hotel);

		return ResponseEntity.ok(updatedHotel);
	}

	// Delete a Hotel

	@DeleteMapping("/hotels/{id}")
	public ResponseEntity<?> deleteHotel(@PathVariable(value = "id") Long id) {
		Hotel hotel = hotelService.getHotelById(id);
		if (hotel == null) {
			log.error("Unable to delete. Hotel with id {} not found.", id);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("Unable to delete. Hotel with id " + id + " not found."), HttpStatus.NOT_FOUND);
		}
		hotelService.deleteHotel(hotel.getId());

		log.info("Region with id {} deleted", id);
		return new ResponseEntity<Void>(HttpStatus.GONE);
	}

	// REGION

	// Get All Regions

	@GetMapping("/regions")
	public ResponseEntity<List<Region>> getAllRegions() {
		log.info("Getting all regions");
		List<Region> regions = hotelService.getAllRegions();
		if (regions.isEmpty()) {
			log.error("Region list is empty!");
			return new ResponseEntity(new CustomErrorType("Region list is empty!"), HttpStatus.NOT_FOUND);
		}
		return ResponseEntity.ok().body(regions);
	}

	// Get a single Region

	@GetMapping("/regions/{id}")
	public ResponseEntity<?> getRegionById(@PathVariable(value = "id") Long id) {
		log.info("Fetching region with id {}", id);
		Region region = hotelService.getRegionById(id);
		if (region == null) {
			log.info("Region with id {} not found", id);
			return new ResponseEntity<CustomErrorType>(new CustomErrorType("Region with id" + id + " not found."),
					HttpStatus.NOT_FOUND);
		}
		return ResponseEntity.ok().body(region);
	}

	// Create a new Region
	@SuppressWarnings("unchecked")
	@PostMapping("/regions")
	public ResponseEntity<?> addRegion(@Valid @RequestBody Region region) throws Exception {

		log.info("Creating Region : {}", region);
		String name = region.getName();
		if (hotelService.isRegionExist(region)) {
			log.error("Unable to create. Region with name {} already exists.", name);
			return new ResponseEntity(
					new CustomErrorType("Unable to create. Region with name " + name + " already exists."),
					HttpStatus.CONFLICT);
		}

		// System.out.println("Region JSON: "+ objectMapper.writeValueAsString(region));
		Region result = hotelService.createRegion(region);

		if (result == null) {
			log.error("Region creation failed!");
			return ResponseEntity.noContent().build();
		} else
			log.info("Created region with name {}", result.getName());

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(result.getId())
				.toUri();

		return ResponseEntity.created(location).build();

	}

	// Update a Region
	@SuppressWarnings("unchecked")
	@PutMapping("/regions/{id}")
	public ResponseEntity<Region> updateRegion(@PathVariable(value = "id") Long id,
			@Valid @RequestBody Region regionDetails) {
		Region region = hotelService.getRegionById(id);
		if (region == null) {
			log.error("Unable to update. Region with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Unable to update. Region with id " + id + " not found."),
					HttpStatus.NOT_FOUND);
		}
		region.setName(regionDetails.getName());

		Region updatedRegion = hotelService.updateRegion(region);

		return ResponseEntity.ok(updatedRegion);
	}

	// Delete a Region
	@SuppressWarnings("unchecked")
	@DeleteMapping("/regions/{id}")
	public ResponseEntity<Void> deleteRegion(@PathVariable(value = "id") Long id) {
		Region region = hotelService.getRegionById(id);
		if (region == null) {
			log.error("Unable to delete. Region with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Unable to delete. Region with id " + id + " not found."),
					HttpStatus.NOT_FOUND);
		}
		hotelService.deleteRegion(id);
		log.info("Region with id {} deleted", id);
		return new ResponseEntity<Void>(HttpStatus.GONE);
	}

}