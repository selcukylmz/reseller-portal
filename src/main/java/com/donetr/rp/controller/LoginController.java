package com.donetr.rp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RestController;

import com.donetr.rp.domain.Hotel;
import com.donetr.rp.domain.Region;
import com.donetr.rp.service.HotelService;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@RestController
public class LoginController {

	@Autowired
	HotelService hotelService;
	
	@GetMapping("/")
	public String index() {
		
		//Region region = new Region("Region 1");
		//region = hotelService.createRegion(region);
		
		//Hotel hotel = new Hotel("Hotel 1", region);
		//hotelService.createHotel(hotel);
		
		//region.setName("Changed Name");
		//hotelService.updateRegion(region);
		
		return "index";
	}

}