package com.donetr.rp.service;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.donetr.rp.domain.Hotel;
import com.donetr.rp.domain.Region;

public interface HotelService {

	// Hotel
	List<Hotel> getAllHotels();

	Hotel getHotelById(Long id);

	Hotel createHotel(Hotel hotel);

	Hotel updateHotel(Hotel hotel);

	void deleteHotel(Long id);

	Hotel getHotelByName(String name);

	List<Hotel> getHotelsByRegion(Region region);
	
	public boolean isHotelExist(Hotel hotel);

	
	// Region
	List<Region> getAllRegions();

	Region getRegionById(Long id);

	Region createRegion(Region region);

	Region updateRegion(Region region);

	void deleteRegion(Long id);

	Region getRegionByName(String name);
	
	public boolean isRegionExist(Region region);


}
