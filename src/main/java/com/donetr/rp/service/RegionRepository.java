package com.donetr.rp.service;

import org.springframework.stereotype.Repository;

import com.donetr.rp.domain.Hotel;
import com.donetr.rp.domain.Region;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface RegionRepository extends JpaRepository<Region, Long> {

	Region findById(Long id);
	Region findByName(String name);
	
}