package com.donetr.rp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.donetr.rp.domain.Hotel;
import com.donetr.rp.domain.Region;
import com.donetr.rp.service.HotelRepository;
import com.donetr.rp.service.RegionRepository;

@Service
public class HotelServiceImpl implements HotelService {

	@Autowired
	HotelRepository hotelRepository;

	@Autowired
	RegionRepository regionRepository;

	@Override
    @Transactional
	public Hotel getHotelById(Long id) {
		return hotelRepository.findOne(id);
	}

	@Override
    @Transactional
	public Hotel createHotel(Hotel hotel) {
		return hotelRepository.saveAndFlush(hotel);
	}

	@Override
    @Transactional
	public Hotel updateHotel(Hotel hotel) {
		return hotelRepository.save(hotel);
	}

	@Override
    @Transactional
	public void deleteHotel(Long id) {
		Hotel hotel = getHotelById(id);
		if (hotel != null) {
			hotelRepository.delete(id);
		}
	}

	@Override
    @Transactional
	public boolean isHotelExist(Hotel hotel) {
		return hotelRepository.findByName(hotel.getName()) != null;
	}

	// REGION

	@Override
    @Transactional
	public Region getRegionById(Long id) {
		return regionRepository.findOne(id);
	}

	@Override
    @Transactional
	public Region createRegion(Region region) {
		System.out.println("CREATING THE REGION");
		return regionRepository.saveAndFlush(region);
	}

	@Override
    @Transactional
	public Region updateRegion(Region region) {
		return regionRepository.save(region);
	}

	@Override
    @Transactional
	public void deleteRegion(Long id) {
		Region region = getRegionById(id);
		if (region != null) {
			regionRepository.delete(id);
		}
	}

	@Override
    @Transactional
	public Hotel getHotelByName(String name) {
		return hotelRepository.findByName(name);
	}

	@Override
    @Transactional
	public List<Hotel> getHotelsByRegion(Region region) {
		return hotelRepository.findByRegion(region);
	}

	@Override
    @Transactional
	public Region getRegionByName(String name) {
		return regionRepository.findByName(name);
	}

	@Override
    @Transactional
	public List<Hotel> getAllHotels() {
		return hotelRepository.findAll();
	}

	@Override
	public List<Region> getAllRegions() {
		return regionRepository.findAll();
	}

	@Override
	public boolean isRegionExist(Region region) {
		return regionRepository.findByName(region.getName()) != null;
	}

}
