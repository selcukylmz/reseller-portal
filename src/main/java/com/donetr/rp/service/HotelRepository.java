package com.donetr.rp.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.donetr.rp.domain.Hotel;
import com.donetr.rp.domain.Region;

@Repository
public interface HotelRepository extends JpaRepository<Hotel, Long> {
	Hotel findById(Long id);

	Hotel findByName(String name);

	List<Hotel> findByRegion(Region region);


}