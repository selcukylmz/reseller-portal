package com.donetr.rp.domain;

import javax.persistence.*;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.validation.constraints.Size;

import java.util.*;
import java.io.Serializable;

@Entity
@Table(name = "region")
public class Region implements Serializable {

	private static final long serialVersionUID = 112312312L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotBlank
	@Size(min = 2, max = 30)
	public String name;

	@OneToMany(mappedBy = "region", cascade = CascadeType.ALL)
	@JsonManagedReference
	public List<Hotel> hotels;

	public Region(String name) {
		this.name = name;
	}

	public Region() {

	}
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Hotel> getHotels() {
		return hotels;
	}

	public void setHotels(List<Hotel> hotels) {
		this.hotels = hotels;
	}

}
