package com.donetr.rp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
//@EnableAutoConfiguration
//@EnableJpaAuditing
public class ResellerPortalApplication {

	public static void main(String[] args) {
		SpringApplication.run(ResellerPortalApplication.class, args);
	}

}
